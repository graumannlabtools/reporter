#' @export
generate_parameters_table <- function(parameters_df)
{
  # Check prerequisites -----------------------------------------------------
  assertive.types::assert_is_data.frame(parameters_df)

  # Process -----------------------------------------------------------------
  # Reformat df
  parameters_df %<>%
  merge(
    y = ReporteR.maxquant::mq_mqpar.xml_fields_used %>%
      dplyr::select('field', 'reporting_block'),
    by.x = 'Parameter',
    by.y = 'field')
  # gt'ize
  parameters_df %>%
    gt::gt(groupname_col = 'reporting_block') %>%
    gt::row_group_order(levels(parameters_df$reporting_block))
}
