generate_mq_fields_used <- function(
  path_to_file = system.file(
    file.path('extdata', 'mq_mqpar.xml_fields_used.ods'),
    package = 'ReporteR.maxquant',
    mustWork = TRUE))
{
  # Check prerequisites -----------------------------------------------------
  assertive.types::assert_is_a_string(path_to_file)
  assertive.files::assert_all_are_readable_files(path_to_file)

  # Read in the data --------------------------------------------------------
  types <- list(
    field                 = readr::col_character(),
    level                 = readr::col_factor(),
    reporting_condition   = readr::col_factor(),
    reporting_dependency  = readr::col_factor(),
    reporting_block       = readr::col_factor(
      levels = c('General', 'Identification', 'Quantitation')),
    reporting_simple      = readr::col_logical())
  mq_mqpar.xml_fields_used <- path_to_file %>%
    readODS::read_ods(
      col_types = readr::as.col_spec(types))

  # Write out R object ------------------------------------------------------
  usethis::use_data(
    mq_mqpar.xml_fields_used,
    overwrite = TRUE)
}