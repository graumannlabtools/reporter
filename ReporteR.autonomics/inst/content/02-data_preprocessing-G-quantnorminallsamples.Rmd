### Quantile Normalization Within All Samples
```{r parameters-and-defaults, include = FALSE}
module <- "autonomics"
section <- "data_preprocessing"
```
```{r parameter-merge, include = FALSE}
local_params <- module %>%
  getOption() %>%
  magrittr::extract2(section) %>%
  ReporteR.base::validate_params(parameters_and_defaults)
```

```{r autonomics-data-preprocessing-G-quantnorminallsamples-processing, include = FALSE}
data_prior_to_quantnorm <- managed_objects$data

# Quantile normalization
managed_objects$data %<>%
  lapply(
    autonomics::preprocess_eset,
    normalize_between_all_samples = TRUE,
    plot = FALSE)
```

Data from Omics analyzes frequently maintains the rank of a given feature within
the distribution of measurements, while the width of that distribution varies
considerably between replicates or subgroups. This negatively impacts differential
expression analysis as performed on this dataset, as it evaluates reproducibility
between replicates and subgroups. As in this case, mapping the data onto a joined
reference distribution while maintaining sample quantiles occasionally mittigates
those effects.
For this analysis, data has thus been mapped within all samples. The effect on
sample distributions may be observed in Figure
\@ref(fig:autonomics-data-preprocessing-G-quantnorminallsamples-viscompB)A, while
Figure \@ref(fig:autonomics-data-preprocessing-G-quantnorminallsamples-viscompB)B
uses **P**rincipal **C**omponent **A**nalysis
`r ifelse(
    'pca' %in% names(params[['autonomics']]),
    '[PCA; see below for more on this technique; @ringner_2008]',
    '[PCA; @ringner_2008]')`,
to highlight the benefit of this approach in the dataset through improved
grouping and separation.

```{r autonomics-data-preprocessing-G-quantnorminallsamples-viscompA, include = FALSE}
# This is split into two chunks ('autonomics-data-preprocessing-invnorm-viscomp[AB]')
# to work around a rmarkdown/bookdown bug that otherwise produces an empty graphics
# object/page
# https://support.rstudio.com/hc/en-us/community/posts/239529128-Notebooks-grid-newpage
color_var <- autonomics.plot::default_color_var(data_prior_to_quantnorm$proteinGroups)
color_values <- autonomics.plot::default_color_values(data_prior_to_quantnorm$proteinGroups, color_var)
autonomics_quantnorm_viscompA <- autonomics.plot::plot_sample_distributions2(
  object = data_prior_to_quantnorm$proteinGroups,
  managed_objects$data$proteinGroups,
  color_var = color_var,
  color_values = color_values,
  facet2_var = c("Input", "Quantile~Normalized"),
  displayed_features = NULL,
  horizontal = TRUE,
  title = "",
  xlab = "Measurement",
  ylab = ""
)
autonomics_quantnorm_viscompB <- autonomics.explore::plot_pca_samples(
  object = data_prior_to_quantnorm$proteinGroups,
  managed_objects$data$proteinGroups,
  color_var = color_var,
  color_values = color_values,
  facet_var = c("Input", "Quantile Normalized")
)
```

```{r autonomics-data-preprocessing-G-quantnorminallsamples-viscomp-parameters, include = FALSE}
fig_height <- ReporteR.base::estimate_figure_height(
  height_in_panels = c(
    managed_objects$data$proteinGroups %>%
      # Number of samples
      ncol() %>%
      # How many panlel heights (samples/panel)?
      magrittr::divide_by(params$formatting_defaults$figures$panel_factor_n) %>%
      round() %>%
      max(1, na.rm = TRUE),
    1),
  panel_height_in_in = params$formatting_defaults$figures$panel_height_in,
  axis_space_in_in = params$formatting_defaults$figures$axis_space_in,
  mpf_row_space = as.numeric(grid::convertUnit(grid::unit(5, 'mm'), 'in')),
  max_height_in_in = params$formatting_defaults$figures$max_height_in)
```

```{r autonomics-data-preprocessing-G-quantnorminallsamples-viscompB, warning = FALSE, message = FALSE, fig.height = fig_height$global, fig.cap = paste0('**A.** Violin plots [@hintze_1998] graphically demonstrating for protein groups data the effect of quantile normalization between all samples as employed in the bioinformatic analysis. **B.** Further exploration of the effect of the normalization in the current analysis through *principal component analysis* and a biplot of principal component 1 (\'X1\') vs. 2 (\'X2\'); note the evolution of association within and separation between groups.')}
# This is split into two chunks ('autonomics-data-preprocessing-invnorm-viscomp[AB]')
# to work around a rmarkdown/bookdown bug that otherwise produces an empty graphics
# object/page
# https://support.rstudio.com/hc/en-us/community/posts/239529128-Notebooks-grid-newpage
multipanelfigure::multi_panel_figure(
  height = fig_height$sub,
  columns = 1,
  unit = "in") %>%
  multipanelfigure::fill_panel(
    autonomics_quantnorm_viscompA,
    row = 1
  ) %>%
  multipanelfigure::fill_panel(
    autonomics_quantnorm_viscompB,
    row = 2
  )
```
