## Differential Expression Analysis {#diff-exp-anal}
```{r parameters-and-defaults, include = FALSE}
module <- "autonomics"
section <- "differential_expression_analysis"
parameters_and_defaults <- list(
  contrasts = structure(
    NA,
    type = "character",
    choices = NA,
    several.ok = TRUE
  ),
  names  = structure(
    "",
    type = "character",
    choices = NA,
    several.ok = TRUE
  ),
  descriptions = structure(
    "",
    type = "character",
    choices = NA,
    several.ok = TRUE
  ),
  selection_criterium = structure(
    "p < 0.05",
    type = "character",
    choices = NA,
    several.ok = FALSE
  ),
  volcano_top_n = structure(
    5.0,
    type = "numeric",
    choices = NA,
    several.ok = FALSE
  )
)
```
```{r parameter-merge, include = FALSE}
local_params <- module %>%
  getOption() %>%
  magrittr::extract2(section) %>%
  ReporteR.base::validate_params(parameters_and_defaults)
```

```{r autonomics-differential-expression-prerequisites, include = FALSE}
design <- managed_objects$data$proteinGroups %>%
  autonomics.find::create_design_matrix()
if(local_params$names != '')
{
  local_params$contrasts %<>%
    magrittr::set_names(local_params$names)
}
local_params$contrasts %<>%
  autonomics.find::validify_contrasts(design)
processed_contrasts <- local_params$contrasts %>%
  ReporteR.base::flag_persistent()

base_has_tst <- 'base' %>%
  options() %>%
  magrittr::extract2('base') %>%
  magrittr::extract2('intro') %>%
  magrittr::extract2('file_format_note') %>%
  magrittr::equals('tab_sep_txt')

preprocessing_is_performed <- 'data_preprocessing' %in% names(getOption(module))
```

Bayesian--moderated t--testing/linear modeling as implemented in the `R` package
[`limma`](https://doi.org/doi:10.18129/B9.bioc.limma) [@richie_2015] is used
by [`autonomics`](https://github.com/bhagwataditya/autonomics) to determine
differential expression/regulation. This methodology is widely applied as well
as respected and also excells in comparisons --- especially when considering
computational resources required
[see e.g. @dangelo_2017; @rapaport_2013; @seyednasrollah_2015].\
\
Comparisons (`contrasts`) performed here are:
`r if (assertive.strings::is_an_empty_string(local_params$descriptions))
{
  sprintf(
    "\n\n%s\n \n",
    paste0("* ", local_params$contrasts, names(local_params$contrasts), ' (encoded in column/field names as \x60', names(local_params$contrasts), '\x60)',collapse = "\n"))
} else {
  stopifnot(
    assertive.properties::are_same_length(
      local_params$contrasts,
      local_params$descriptions))
  sprintf("\n\n%s\n \n", paste0('* ', local_params$contrasts, ': ', local_params$descriptions,  ' (encoded in column/field names as \x60', names(local_params$contrasts), '\x60)', collapse = "\n"))
}`

The smaller a calculated $p$ value for any feature in any of these comparisons,
the more likely differential expression/regulation in the context of the contrast
analyzed is. Note that `limma` internally penalizes features with small effect size
but high reproducibility.

Given the omic nature of the analysis and the implied plethora of tests performed,
interpretation of resulting $p$ values benefits from correction for multiple
hypothesis testing [@noble_2009]. Next to their raw, uncorrected version,
[`autonomics`](https://github.com/bhagwataditya/autonomics) thus reports $p$
values corrected by two different methodologies: the extremely conservative
*Bonferroni* method[^2] (corresponding columns/fields are labeled `bonf`) and the
more forgiving approach proposed by @benjamini_1995[^3] (corresponding columns/fields
in the output are labeled `FDR`).

[^2]: Filtering at a given corrected $p$ allows to control of the *familywise error rate*,
      "... which is the probability of at least one false positive among the [features]
      selected as differentially expressed ..." [@smyth_2003].
[^3]: @smyth_2003 "... argue[...] that controlling the familywise error rate is
      unnecessarily stringent [...], because falsely selecting a handful of [features]
      as differentially expressed will not be a serious problem if the majority of significant
      [features] are correctly chosen. A less stringent and therefore more powerful method is to
      control the false discovery rate, defined to be the  expected proportion of errors among
      the [features] selected as significantly differentially expressed." One approach towards 
      that end was proposed by @benjamini_1995, and implemented for $p$ value adjustment in
      `R`'s `p.adjust` function by Gordon Smyth.

**For most intents and purposes corrected $p$ values listed in columns/fields labeled `FDR`
should be used along with the corresponding `coef`(ficient). The latter  may be interpreted
as the effect (fold change in the case of subgroup comparisons).**
```{r autonomics-differential-expression-analysis-A-preprocessing, echo = FALSE, child = system.file(file.path('content', '04-differential_expression_analysis-A-preprocessing.Rmd'), package = 'ReporteR.autonomics', mustWork = TRUE), R.options = params, eval = ifelse(exists('preprocessing_is_performed'), preprocessing_is_performed, FALSE)}
```

```{r autonomics-differential-expression-run-limma, include = FALSE}
objects_to_treat <- c('proteinGroups', 'phosphoSites', 'phosphoOccupancies') %>%
  magrittr::extract(
    c(TRUE, managed_objects$switches$phospho, managed_objects$switches$phospho_occupancy))
# Mimicing a subset of 'autonomics::analyze_contrasts'
## Run limma
managed_objects$data %<>%
  lapply(
    autonomics.find::add_limma_to_fdata,
    contrasts = local_params$contrasts,
    design = design,
    overwrite = TRUE)
## Write out results
managed_objects$paths$autonomics_intro$result_dir_abs %>%
  lapply(
    autonomics.find::get_contrast_subdir,
    contrast_name = local_params$contrasts %>%
      names()) %>%
  unlist() %>%
  as.list() %>%
  plyr::l_ply(dir.create, showWarnings = FALSE, recursive = TRUE)
for(obj in names(managed_objects$data))
{
  autonomics.find::write_features(
    object = managed_objects[['data']][[obj]], 
    design = design,
    contrasts = local_params$contrasts,
    top_definition = local_params$selection_criterium,
    result_dir = managed_objects$paths$autonomics_intro$result_dir_abs[obj])
}

## Plot top features
for(obj in names(managed_objects$data))
{
  autonomics.find::plot_top_features_all_contrasts(
    object = managed_objects[['data']][[obj]],
    design = design,
    contrasts = local_params$contrasts,
    result_dir = managed_objects$paths$autonomics_intro$result_dir_abs[obj],
    top_definition = local_params$selection_criterium,
    feature_plots = autonomics.plot::default_feature_plots(
      managed_objects[['data']][[obj]]),
    x = autonomics.plot::default_x(
      object = managed_objects[['data']][[obj]],
      autonomics.plot::default_feature_plots(managed_objects[['data']][[obj]])[1]),
    color_var = autonomics.plot::default_color_var(
      managed_objects[['data']][[obj]]),
    color_values = autonomics.plot::default_color_values(
      managed_objects[['data']][[obj]]),
    shape_var = autonomics.plot::default_shape_var(
      managed_objects[['data']][[obj]]),
    group_var = autonomics.plot::default_group_var(
      managed_objects[['data']][[obj]]),
    facet_var = autonomics.plot::default_facet_var(),
    line = autonomics.plot::default_line(
      managed_objects[['data']][[obj]]),
    fvars = autonomics.plot::default_fvars(
      managed_objects[['data']][[obj]]),
    nplot = autonomics.find::default_nplot(
      managed_objects[['data']][[obj]]),
    width = NULL,
    height = NULL)
}
all_features_table_path_disp <- managed_objects$paths$autonomics_intro$result_dir_rel_disp %>%
  paste("all_feature_table.txt", sep = '/') %>%
  magrittr::set_names(names(managed_objects$paths$autonomics_intro$result_dir_rel_disp))
all_features_table_path_abs <- managed_objects$paths$autonomics_intro$result_dir_abs %>%
  file.path('all_feature_table.txt') %>%
  magrittr::set_names(names(managed_objects$paths$autonomics_intro$result_dir_abs))
managed_objects$paths$paths_for_packaging_abs %<>%
  c(all_features_table_path_abs) %>%
  ReporteR.base::flag_persistent()
contrast_dir_path_rel_disp <- paste(
  managed_objects$paths$autonomics_intro$result_dir_rel_disp,
  "contrasts", ".", sep = '/') %>%
  magrittr::set_names(names(managed_objects$paths$autonomics_intro$result_dir_rel_disp)) %>%
  ReporteR.base::flag_persistent()
contrast_dir_path_abs <- managed_objects$paths$autonomics_intro$result_dir_abs %>%
  file.path("contrasts", ".") %>%
  magrittr::set_names(names(managed_objects$paths$autonomics_intro$result_dir_abs)) %>%
  ReporteR.base::flag_persistent()
managed_objects$paths$paths_for_packaging_abs %<>%
  c(
    contrast_dir_path_abs %>%
      list.files(
        pattern = "top_feature_table_",
        recursive = TRUE,
        full.names = TRUE),
    contrast_dir_path_abs %>%
      list.files(
        pattern = "top_.*\\.pdf",
        recursive = TRUE,
        full.names = TRUE)) %>%
  ReporteR.base::flag_persistent()
```

```{r autonomics-differential-expression-volcano-plot-parameters, include = FALSE}
height_in_panels = local_params$contrasts %>%
    length() %>%
    magrittr::divide_by(2) %>%
    round() %>%
    max(1, na.rm = TRUE)
fig_height <- ReporteR.base::estimate_figure_height(
  height_in_panels = height_in_panels,
  panel_height_in_in = params$formatting_defaults$figures$panel_height_in * 1.5, # need more real estate for labels
  axis_space_in_in = params$formatting_defaults$figures$axis_space_in,
  mpf_row_space = as.numeric(grid::convertUnit(grid::unit(5, 'mm'), 'in')),
  max_height_in_in = params$formatting_defaults$figures$max_height_in)
```

```{r autonomics-differential-expression-volcano-plot, fig.height=fig_height$global, fig.cap = paste0('*Volcano plots* [@cui_2003] summarizing the differential expression analysis **on the protein groups level** using [`limma`](https://doi.org/doi:10.18129/B9.bioc.limma) [@richie_2015] per `contrast` queried. As *multiple hypothesis testing correction* yields many $p=1.00$ and implies a discontinuous graphical representation, raw $p$ values are plotted as $-log_{10}(p)$ on the y-- and coefficients (interpretable as average fold changes) on the x--axis. The scatter plot is colored according to fold change/FDR--corected $p$ values [@benjamini_1995]. Strong candidate features in either direction are labeled  using protein names (occasionally at the expense of readability).')}
managed_objects$data$proteinGroups %>%
  autonomics.plot::plot_volcano(
    ntop = local_params$volcano_top_n,
    legend_position = if(length(local_params$contrasts) > 1){"top"}else{NULL},
    nrow = height_in_panels)
```

Figure \@ref(fig:autonomics-differential-expression-volcano-plot) provides a graphical
overview of the protein groups--level results in the form of *volcano plots* [@cui_2003].

For the protein groups data, detailed results for all `contrasts` are written to
[this file (`all_features_table.txt`)](`r all_features_table_path_disp['proteinGroups']`)
(for help with the format see Section \@ref(file-format-general)).
[This accompanying directory](`r contrast_dir_path_rel_disp['proteinGroups']`)
additionally contains `contrast`--specific subdirectories with individual
result tables and graphical representations of the most significantly changing
features. File names containting `*pos*` and `*neg*` respectively indicate
separation into features up or down regulated in the context of the `contrast`.

```{r autonomics-differential-expression-analysis-B-phospho, echo = FALSE, child = system.file(file.path('content', '04-differential_expression_analysis-B-phospho.Rmd'), package = 'ReporteR.autonomics', mustWork = TRUE), R.options = params, eval = ifelse(exists('managed_objects'), managed_objects$switches$phospho, FALSE)}
```

```{r autonomics-differential-expression-analysis-B-phosphooccupancies, echo = FALSE, child = system.file(file.path('content', '04-differential_expression_analysis-C-phosphooccupancies.Rmd'), package = 'ReporteR.autonomics', mustWork = TRUE), R.options = params, eval = ifelse(exists('managed_objects'), managed_objects$switches$phospho_occupancy, FALSE)}
```

### Step--by--Step Recipe to Data Navigation (Differential Expression Analysis) {#step-by-step-diffex-navigation}
To explore the data relating to the differential expression analysis of a particular
comparison/contrast the following steps may be followed (the example refers to the protein
groups data set):

1. Select the encoding (naming) for a comparison (`contrast`) of choice from the list above (referenced as
    `<CONTRAST>` from here on).
   
2. Access [`all_features_table.txt`](`r all_features_table_path_disp['proteinGroups']`)  using e.g. `Microsoft Excel`
    `r if(base_has_tst){"(see Section \\@ref(file-format-excel) for more)."} else {"."}`

3. Reduce displayed columns (by hiding or deletion) to:

    a. Information on protein groups including `feature_id`, `Uniprot accessions`,
        `Protein names`, `Gene names` and `Fasta headers`
      
    b. `contrast`--specific information: `rank.<CONTRAST>`, `coef.<CONTRAST>`,\
        `quantile.<CONTRAST>`, `p.<CONTRAST>`, `fdr.<CONTRAST>` and `bonf.<CONTRAST>`.

4. Sort by `rank.<CONTRAST>` (ascending) to order protein groups by likelyhood of
    differential expression.

5. Evaluate `fdr.<CONTRAST>`, which represents the $p$--value corrected for multiple hypothesis 
    testing using the methodology by  @benjamini_1995 (see above). If this value is
    $p_{FDR} \le 0.01$, differential expression is highly significant (in the comparison/contrast).
    in question, while $p_{FDR} \le 0.05$ implies a merely significant change.

6. The direction of the change and the effect size are represented by the column `coef.<CONTRAST>`.
    This is most often interpretable as a *log fold change* (implying prior logarithmization; see
    appropriate section). Where data have been further transformed/normalized, the interpretation is
    less straight forward.
    
    The arithmetic sign ($+$ or $-$) encodes the direction of a given regulation. The larger the
    absolute numeric value, the stronger the effect.

```{r autonomics-differential-expression-terminal-cleanup, include = FALSE}
ReporteR.base::purge_nonpersistent()
```
