```{r parameters-and-defaults, include = FALSE}
module <- "autonomics"
section <- "data_preprocessing"
```

```{r parameter-merge, include = FALSE}
local_params <- module %>%
  getOption() %>%
  magrittr::extract2(section) %>%
  ReporteR.base::validate_params(parameters_and_defaults)
```
Logarithmization is particularly important in the context of the employed
quantitation approach, as upstream processing of raw data produces ratios
between conditions compared. Fractions with enumerator abundances exceeding
denominator ones are thus represented by values between $1.0$ and infinity,
while the corresponding inverse ratios are compressed between $1.0$ and $0.0$.
Logarithmization addresses this asymetry.
