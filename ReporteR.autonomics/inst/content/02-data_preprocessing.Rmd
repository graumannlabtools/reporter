## Data Preprocessing
```{r parameters-and-defaults, include = FALSE}
module <- "autonomics"
section <- "data_preprocessing"
parameters_and_defaults <- list(
  zero_is_na = structure(
    FALSE,
    type = 'logical',
    choices = NA,
    several.ok = FALSE
  ),
  imputation = structure(
    'none',
    type = 'character',
    choices = c('none', 'impute.MinDet', 'impute.MinProb', 'impute.QRILC'),
    several.ok = FALSE
  ),
  imputation_random_seed = structure(
    22041724,
    type = 'numeric',
    choices = NA,
    several.ok = FALSE
  ),
  log = structure(
    TRUE,
    type = "logical",
    choices = NA,
    several.ok = FALSE
  ),
  log_base = structure(
    2,
    type = "numeric",
    choices = NA,
    several.ok = FALSE
  ),
  log_plus_one = structure(
    FALSE,
    type = 'logical',
    choices = NA,
    several.ok = FALSE
  ),
  compare_normalizations = structure(
    TRUE,
    type = "logical",
    choices = NA,
    several.ok = FALSE
  ),
  invnormtrans_in_samples = structure(
    FALSE,
    type = "logical",
    choices = NA,
    several.ok = FALSE
  ),
  quantnorm_in_subgroup_samples = structure(
    FALSE,
    type = "logical",
    choices = NA,
    several.ok = FALSE
  ),
  quantnorm_in_all_samples = structure(
    FALSE,
    type = "logical",
    choices = NA,
    several.ok = FALSE
  )
)
```
```{r parameter-merge, include = FALSE}
local_params <- module %>%
  getOption() %>%
  magrittr::extract2(section) %>%
  ReporteR.base::validate_params(parameters_and_defaults)
```

```{r autonomics-data-preprocessing-A-zeroisna, echo = FALSE, child = system.file(file.path('content', '02-data_preprocessing-A-zeroisna.Rmd'), package = 'ReporteR.autonomics', mustWork = TRUE), R.options = params, eval = ifelse(exists('local_params'), local_params$zero_is_na, FALSE)}
```

```{r autonomics-data-preprocessing-B-imputation, echo = FALSE, child = system.file(file.path('content', '02-data_preprocessing-B-imputation.Rmd'), package = 'ReporteR.autonomics', mustWork = TRUE), R.options = params, eval = ifelse(exists('local_params'), local_params$imputation != 'none', FALSE)}
```

```{r autonomics-data-preprocessing-C-log, echo = FALSE, child = system.file(file.path('content', '02-data_preprocessing-C-log.Rmd'), package = 'ReporteR.autonomics', mustWork = TRUE), R.options = params, eval = ifelse(exists('local_params'), local_params$log, FALSE)}
```

```{r autonomics-data-preprocessing-D-normcompare, echo = FALSE, child = system.file(file.path('content', '02-data_preprocessing-D-normcompare.Rmd'), package = 'ReporteR.autonomics', mustWork = TRUE), R.options = params, eval = ifelse(exists('local_params'),local_params$compare_normalizations, FALSE)}
```

```{r autonomics-data-preprocessing-E-invnormtransinsamples, echo = FALSE, child = system.file(file.path('content', '02-data_preprocessing-E-invnormtransinsamples.Rmd'), package = 'ReporteR.autonomics', mustWork = TRUE), R.options = params, eval = ifelse(exists('local_params'), local_params$invnormtrans_in_samples, FALSE)}
```

```{r autonomics-data-preprocessing-F-quantnorminsubgroups, echo = FALSE, child = system.file(file.path('content', '02-data_preprocessing-F-quantnorminsubgroups.Rmd'), package = 'ReporteR.autonomics', mustWork = TRUE), R.options = params, eval = ifelse(exists('local_params'), local_params$quantnorm_in_subgroup_samples, FALSE)}
```

```{r autonomics-data-preprocessing-G-quantnorminallsamples, echo = FALSE, child = system.file(file.path('content', '02-data_preprocessing-G-quantnorminallsamples.Rmd'), package = 'ReporteR.autonomics', mustWork = TRUE), R.options = params, eval = ifelse(exists('local_params'), local_params$quantnorm_in_all_samples, FALSE)}
```

```{r autonomics-data-preprocessing-terminal-cleanup, include = FALSE}
ReporteR.base::purge_nonpersistent()
```

