### Differential Regulation in the Phospho--Occupancy Dataset
```{r parameter-merge, include = FALSE}
module <- "autonomics"
section <- "ora"
local_params <- module %>%
  getOption() %>%
  magrittr::extract2(section) %>%
  ReporteR.base::validate_params(parameters_and_defaults)
```
ORA analysis was performed parallely to the protein groups data.
**Note, that bioinformatic downstream analysis was performed on quantitative
data for occupancies derived from singly phosphorylated peptides ONLY.**

For the phoshpo--occupancy dataset, detailed results for all `contrasts` analyzed
are collected and written to `ora_in_*` subdirectories of the `contrast`--specific subdirectories found [here](`r contrast_dir_path_rel_disp['phsophoOccupancies']`) with
individual result tables (for help with the format see Section \@ref(file-format-general)).
File names start with the ontology(subsection) analyzed (e.g. `kegg_*` or
`gobp_*`), continue with the corresponding `contrast` name and end in
`*pos*`, `*neg*` or `*both*`, respectively indicating whether the tested
features where up, down or generally regulated in the context of the
`contrast`.