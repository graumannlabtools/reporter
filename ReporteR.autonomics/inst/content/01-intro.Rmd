# Downstream Bioinformatic Analysis
```{r parameters-and-defaults, include = FALSE}
module <- "autonomics"
section <- "intro"
parameters_and_defaults <- list(
  maxquant_dir = structure(
    NA,
    type = "character",
    choices = NA,
    several.ok = FALSE
  ),
  phospho = structure(
    FALSE,
    type = 'logical',
    choices = NA,
    several.ok = FALSE
  ),
  phospho_occupancy = structure(
    FALSE,
    type = 'logical',
    choices = NA,
    several.ok = FALSE
  )
)
```
```{r parameter-merge, include = FALSE}
local_params <- module %>%
  getOption() %>%
  magrittr::extract2(section) %>%
  ReporteR.base::validate_params(parameters_and_defaults)
```
Striving for robust, fully scriptable and reproducible data exploitation,
an in--house developed `R`--based [@r_core_team_2018] data analytical
pipeline named [`autonomics`](https://github.com/bhagwataditya/autonomics)
(manuscript in preparation) was used to process quantitative data for downstream
bioinformatic analysis.

For details such as data preprocessing and more specialized topics, please refer
to the Appendix, Section \@ref(bioinformatics-details).

```{r autonomics-intro-prerequisites, include = FALSE}
managed_objects$paths$autonomics_intro <- list(
  maxquant_dir = local_params$maxquant_dir)
psty_exists <- file.exists(
  file.path(
    managed_objects$paths$autonomics_intro$maxquant_dir,
    "Phospho (STY)Sites.txt"))
managed_objects$switches$phospho <- local_params$phospho && psty_exists
managed_objects$switches$phospho_occupancy <- local_params$phospho_occupancy && psty_exists
```

```{r autonomics-intro-parsedata, include = FALSE}
# Load data into SummarizedExperiment' using autonomics
if(!'data' %in% names(managed_objects)){ managed_objects$data = list() }

pg_path <- file.path(
  managed_objects$paths$autonomics_intro$maxquant_dir,
  "proteinGroups.txt")
managed_objects$data$proteinGroups <- autonomics.import::load_proteingroups(
  proteingroups_file = pg_path,
  log2_transform = FALSE)
managed_objects$paths$paths_for_packaging_abs %<>%
  c(
    pg_path,
    file.path(
      managed_objects$paths$autonomics_intro$maxquant_dir,
      c('parameters.txt', 'mqpar.xml')))

if(any(managed_objects$switches$phospho, managed_objects$switches$phospho_occupancy))
{
  psty_path <- file.path(
    managed_objects$paths$autonomics_intro$maxquant_dir,
    "Phospho (STY)Sites.txt")
  managed_objects$paths$paths_for_packaging_abs %<>%
    c(psty_path)
}

if(managed_objects$switches$phospho)
{
  
  managed_objects$data$phosphoSites <- autonomics.import::load_phosphosites(
    phosphosites_file = psty_path,
    min_loc_prob = 0,
    log2_transform = FALSE)
}
if(managed_objects$switches$phospho_occupancy)
{
  managed_objects$data$phosphoOccupancies <- autonomics.import::load_phosphosite_occupancies(
    phosphosites_file = psty_path,
    min_loc_prob = 0,
    log2_transform = FALSE)
  managed_objects %<>% ReporteR.base::listing(
    path = c("stash", "phosphoOccupanciesLog2"),
    autonomics.import::load_phosphosite_occupancies(
      phosphosites_file = psty_path,
      min_loc_prob = 0,
      log2_transform = TRUE))
}

# Ensure absence of 'NaN'
managed_objects$data %<>%
  lapply(
    function(obj)
    {
      autonomics.import::exprs(obj)[is.nan(autonomics.import::exprs(obj))] <- NA
      return(obj)
    })

# Extract/process metadata for reporting
# prepro_attr <- attr(managed_objects$data$proteinGroups, 'metadata')[['prepro']]

managed_objects$stats <- list(autonomics_intro = list())
## All metadata
managed_objects$stats$autonomics_intro$analysis_attr <- names(managed_objects$data) %>%
  lapply(
    function(obj){
      attr(managed_objects[['data']][[obj]], 'metadata')[['analysis']]
    }) %>%
  magrittr::set_names(names(managed_objects$data))
## Count w/o rev/cont
managed_objects$stats$autonomics_intro$no_rev_cont <- c(
  proteinGroups = managed_objects$stats$autonomics_intro$analysis_attr$proteinGroups %>%
    magrittr::extract(c("proteingroups_n_rmcontaminants", "proteingroups_n_rmreverse")) %>%
    unlist() %>%
    min(na.rm = TRUE))
for(obj in stringi::stri_subset_regex(names(managed_objects$data), pattern = '^phospho'))
{
  managed_objects[['stats']][['autonomics_intro']][['no_rev_cont']][obj] <- managed_objects[['stats']][['autonomics_intro']][['analysis_attr']][[obj]] %>%
    magrittr::extract(c("phosphosites_n_rmcontaminants", "phosphosites_n_rmreverse")) %>%
    unlist() %>%
    min(na.rm = TRUE)
}
# Generate paths for autonomics analysis
managed_objects$paths$autonomics_intro$result_dir_rel <- managed_objects$data %>%
  sapply(
    function(obj){
      file.path(
        ".",
        obj %>%
          autonomics:::default_result_dir())
    })
managed_objects$paths$autonomics_intro$result_dir_rel_disp <- paste0(
  'run:',
  managed_objects$paths$autonomics_intro$result_dir_rel %>%
    stringi::stri_replace_all_fixed(.,'\\','/')) %>%
  magrittr::set_names(names(managed_objects$paths$autonomics_intro$result_dir_rel))
managed_objects$paths$autonomics_intro$result_dir_abs <- file.path(
  managed_objects$paths$output_dir,
  managed_objects$paths$autonomics_intro$result_dir_rel) %>%
  magrittr::set_names(names(managed_objects$paths$autonomics_intro$result_dir_rel))
```

```{r autonomics-intro-datachecks, include = FALSE}
# Ensure use of MQ requantification in ratio data
if(autonomics.import::contains_ratios(managed_objects$data$proteinGroups)){
  if(!ReporteR.autonomics::mq_requantified(file.path(managed_objects$paths$autonomics_intro$maxquant_dir, "mqpar.xml")))
  {
    stop("Dataset contains ratios, but does not appear to use MaxQuant's requantification feature (required here).")
  }
}
```

The underlying dataset consists of
`r managed_objects$stats$autonomics_intro$analysis_attr$proteinGroups$proteingroups_n`
features (protein groups). After removing common contaminants and remnants of
false discovery estimation by a target/decoy database searching approach
[@elias_2007], `r managed_objects$stats$autonomics_intro$no_rev_cont['proteinGroups']` of
these are retained,
`r round(nrow(autonomics.import::fdata(managed_objects$data$proteinGroups))/managed_objects$stats$autonomics_intro$no_rev_cont['proteinGroups']*100)`%
of which are quantified in at least one sample and thus form the basis of the
following analyzes.

```{r autonomics-intro-A-phospho, echo = FALSE, child = system.file(file.path('content', '01-intro-A-phospho.Rmd'), package = 'ReporteR.autonomics', mustWork = TRUE), R.options = params, eval = ifelse(exists('managed_objects'), managed_objects$switches$phospho, FALSE)}
```

```{r autonomics-intro-B-phosphooccupancies, echo = FALSE, child = system.file(file.path('content', '01-intro-B-phosphooccupancies.Rmd'), package = 'ReporteR.autonomics', mustWork = TRUE), R.options = params, eval = ifelse(exists('managed_objects'), managed_objects$switches$phospho_occupancy, FALSE)}
```

```{r autonomics-intro-terminal-cleanup, include = FALSE}
ReporteR.base::purge_nonpersistent()
```
