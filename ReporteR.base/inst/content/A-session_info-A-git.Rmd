## Availability of Analysis Code
```{r parameters-and-defaults, eval = FALSE}
```

```{r base-appendix-sessioninfo-A-git-remote, include=FALSE}
if(managed_objects$switches$is_gitted) remote_url <- ReporteR.base::get_git_remote_url(managed_objects$paths$output_dir)
```

Code and data underlying this report are tracked using the *version control
system* [@taschuk_2017] [`git`](https://git-scm.com/) and hosted 
`r ifelse(local_params$git_repo_private, 'inaccessible', 'accessible')` to
the public at

[` `r remote_url` `](`r remote_url`)

`r ifelse(local_params$git_repo_private, 'Access to the repository is granted upon request.', '')`
