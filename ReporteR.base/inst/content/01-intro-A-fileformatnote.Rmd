## **READ THIS** --- Note on File Format for Tabular Data  {#file-format-general}
```{r parameters-and-defaults, include = FALSE}
module <- "base"
section <- "intro"
parameters_and_defaults <- list(
  file_format_note = structure(
    'tab_sep_txt',
    type = "character",
    choices = c('tab_sep_txt', 'none'),
    several.ok = FALSE)
)
```
Limitations in the file format of `Microsoft Excel` render it unsuitable to reliably
share data [@zeeberg_2004; @linke_2009; @ziemann_2016]. This is particularly true in
a multilingual setting.

Examples for complications include the interpretation of fractional numbers across
language settings ($3.25 \neq 3,25$ in English vs. German language settings),
unintended automated reformatting of scientific notation (*e.g.* `2.67E-10`, small
$p$ values) and conversion of gene names to dates ("Oct4" and "Sep7" are famous examples).

<!-- Currently only implementing 'tab_sep_txt'
{r base-intro-fileformatnote-1, echo = FALSE, child = system.file(file.path('content', '01-intro-A-fileformatnote-.Rmd'), package = 'ReporteR.base', mustWork = TRUE), R.options = params, eval = (local_params$file_format_note == 'tab_sep_txt')} -->

Tabular material is thus provided in plain text files with the file name extension `*.txt`.
The first line of the content encodes the table header and columns are separated using *tab
stops* (*tab--delimited* text files). In the context of fractional numbers, a "`.`" (*dot*,
full stop) is used as the decimal seperator.

The responsibility for ensuring faithful representation of these data by software rests with
the user. Operation from files rendered read--only and spot--checking of correct content
representation using a text editor such as [`NotePad++`](https://notepad-plus-plus.org/)
(`Windows` only) or [`Sublime Text`](https://www.sublimetext.com/) are advised.

### File Access from within `Microsoft Excel` {#file-format-excel}
From **within** `Microsoft Excel`, files may be accessed via the dialog for opening
files (ensuring that not just `Excel`--native file formats are displayed).
Instruct the import wizzard to interpret the `tab` character as the column separator and
check/adjust column format as appropriate. **Approximately correct recognition of numeric
content etc. --- at least under `Microsoft Windows` --- requires the operating system's
language/internationalization options to be set to "`English (US)`".**

If unsure, request support from your IT department.

```{r base-intro-A-terminal-cleanup, include = FALSE}
ReporteR.base::purge_nonpersistent()
```