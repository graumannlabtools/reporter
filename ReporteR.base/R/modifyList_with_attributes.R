#' This is a modified version of utils::modifyList
#' @param x         A named \code{\link{list}}, possibly empty.
#' @param val       A named \code{\link{list}} with components to replace corresponding components in x.
#' @seealso \code{\link[utils]{modifyList}}
#' @examples
#' my_x <- list(
#'   One = structure(
#'     list(),
#'     type = 'list',
#'     choices = NA,
#'     several.ok = TRUE),
#'   Two = structure(
#'     '',
#'     type = 'character',
#'     choices = NA,
#'     several.ok = FALSE),
#'   Three = structure(
#'     list(),
#'     type = 'list',
#'     choices = NA,
#'     several.ok = FALSE),
#'   Four = structure(
#'     'fungi',
#'     type = 'character',
#'     choices = NA,
#'     several.ok = FALSE))
#' my_val <- list(
#'   One = list(
#'     One_One = list(
#'       animals = list(
#'         cat = "nice",
#'         dog = "nasty",
#'         tiger = "scary",
#'         yeti = NULL),
#'       plants = c("tree", "bush", "grass"))),
#'   Two = NULL,
#'   Four = 'archaea')
#'
#' str(modifyList_with_attributes(my_x, my_val))
#' @export
modifyList_with_attributes <- function(
                                       x,
                                       val) {
  stopifnot(is.list(x), is.list(val))
  xnames <- names(x)
  vnames <- names(val)
  vnames <- vnames[nzchar(vnames)]
  for (v in vnames) {
    saved_attributes <- attributes(x[[v]])
    saved_names <- names(x[v])
    if (v %in% xnames && is.list(x[[v]]) && is.list(val[[v]])) {
        x[v] <- list(modifyList_with_attributes(x[[v]], val[[v]]))
    } else {
      x[v] <- val[v]
    }
    if(length(saved_names) == 0 || is.na(saved_names))
    {
      attributes(x[v]) <- saved_attributes
    } else {
      attributes(x[v]) <- c(
        name = saved_names,
        saved_attributes)
    }
  }
  return(x)
}
