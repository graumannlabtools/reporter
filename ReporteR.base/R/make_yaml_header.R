#' @name make_yaml_header
#' @title Generate a \code{YAML} header targeting \code{bookdown}-mediated
#' \pkg{knitr}ing
#' @description \describe{
#' \item{\code{make_yaml_header}:}{This function assembles a
#' \code{YAML} header-formated \code{\link{character}} object from
#' \code{yaml_header_base_content}.}
#' \item{\code{yaml_header_base_content}:}{This function returns a nested
#' \code{\link{list}} used through \code{make_yaml_header} by
#' \code{\link{assemble_reporting_template}} to give sensible default values in
#' the \code{YAML} header of a report destined for \code{bookdown}-mediated
#' \code{knitr}ting.}}
#' @param header_list A \code{\link{list}} object to be formatted as a
#' \code{YAML} header.
#' @return \describe{
#' \item{\code{make_yaml_header}:}{A \code{YAML} header-formated
#' \code{\link{character}} object}
#' \item{\code{yaml_header_base_content}:}{A nested \code{\link{list}} for
#' processing by
#' \code{\link{assemble_reporting_template}}/\code{\link{make_yaml_header}}/\code{\link[yaml]{as.yaml}}.}}
#' @seealso \code{\link{assemble_reporting_template}}, \code{\link[yaml]{as.yaml}}
#' @examples
#' str(yaml_header_base_content())
#' make_yaml_header
#' @export
make_yaml_header <- function(header_list = yaml_header_base_content) {
  header_list %>%
    yaml::as.yaml(indent.mapping.sequence = TRUE) %>%
    paste0("---\n", ., "---") %>%
    return()
}

utils::globalVariables('.')

#' @rdname make_yaml_header
#' @export
yaml_header_base_content <- function() {
  list(
    title = "Project Report",
    subtitle = "POIGNANT DESCRIPTION OF THE WORK AT HAND",
    date = "`r format(Sys.time(), \"%d %B, %Y\")`",
    author = 'John Doe',
    output = list(
      `bookdown::pdf_document2` = list(
        includes = list(
          in_header = 'tex_customizations/00_appendix.tex'),
        template = NULL,
        toc = TRUE,
        toc_depth = 3L
      ),
      `bookdown::word_document2` = list(
        toc = TRUE,
        toc_depth = 3L
      )
    ),
    colorlinks = TRUE,
    fontsize = "11pt",
    documentclass = "scrartcl",
    geometry = "margin=1in, a4paper",
    bibliography = "bibliography.bib",
    `biblio-style` = "https://www.zotero.org/styles/cell",
    `link-citations` = TRUE,
    params = list(
      formatting_defaults = list(
        value = list(
          figures = list(
            axis_space_in = 0.2,
            max_height_in = 9,
            panel_factor_n = 6,
            panel_height_in = 2.25
          )
        )
      )
    )
  ) %>%
    return()
}
