#' An utitlity function to simplify lists
#'
#' The function aims at \code{\link{list}}s generated by
#' \code{\link[jsonlite]{read_json}}, which often harbour needlessly complex
#' leaf nodes. In  particular superflously wrapping terminal \code{\link{list}}
#' structures are being stripped using \code{\link{unlist}}.
#' @param lst A \code{\link{list}} object to be simplified;
#' @author Johannes Graumann
#' @export
clean_up_list <- function(lst)
{
  # Operate incrementally on steps to prevent excessive recurssion &
  # C stack limit issues
  list_depth <- lst %>%
    purrr::vec_depth()
  for(dl in rev(sequence(list_depth)))
  {
    lst %<>%
      purrr::modify_depth(
        .depth = dl, .ragged = TRUE,
        .f = function(x)
        {
          if(is.list(x) && length(x) == 1 && length(x[[1]]) == 1)
          {
            unlist(x, use.names = FALSE)
          } else {
            x
          }
        })
  }
  return(lst)
}
