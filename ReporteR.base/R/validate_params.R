#' validate_params
#'
#' A helper function to check an input (parameter) list against a
#' template/defaults one.
#' @param parameter_list \code{link{list}} object to be checked.
#' @param reference_list \code{link{list}} object supplying the formating
#' information to be enforced.
#' @examples
#' parameters_and_defaults <- list(
#'   file_format_note = structure(
#'     'tab_sep_txt',
#'     type = 'character',
#'     choices = c('tab_sep_txt', 'none'),
#'     several.ok = FALSE),
#'   some_list = structure(
#'     list(),
#'     type = 'list',
#'     choices = NA,
#'     several.ok = TRUE))
#'
#' validate_params(
#'   list(
#'     file_format_note = 'none',
#'     some_list = list(a = 1, b =2)),
#'   parameters_and_defaults)
#' @export
validate_params <- function(
                            parameter_list,
                            reference_list) {
  # Check prerequisites -----------------------------------------------------
  parameter_list %>%
    assertive.types::assert_is_list()

  reference_list %>%
    assertive.types::assert_is_list()

  # Processing --------------------------------------------------------------
  # Merge the lists (while maintaining attributes)
  output <- reference_list %>%
    modifyList_with_attributes(
      val = parameter_list)

  # Process the result recursively
  output %>%
    rapply(
      function(x) {
        ## Extract internally used attributes
        type <- attr(x, "type")
        choices <- attr(x, "choices")
        sok <- attr(x, "several.ok")
        ## Only act on terminal entries (with correct attribute(s))
        if (!is.null(type)) {
          ## Fail on values that are required (do not have a default)
          assertive.base::assert_all_are_not_na(x)
          ## Inheriting properly?
          assertive.types::assert_is_inherited_from(x, type)
          ## Match args
          if (!anyNA(choices)) {
            x %<>%
              match.arg(
                choices = choices,
                several.ok = sok
              )
            ## Isolated length test
          } else if (!sok) {
            assertive.properties::assert_is_of_length(x, 1)
          }
        }
        ## Clear the attributes
        attributes(x) <- NULL
        return(x)
      },
      how = "list"
    ) %>%
    return()
}
