#' @title estimate_figure_heigth
#' @description Deprecated functionality to determine figure height.
#' @param height_in_panels Number of panels to be used.
#' @param panel_height_in_in Height of a panel in inches.
#' @param axis_space_in_inch Amount of space reserved for an axis in inches.
#' @param mpf_row_space Inter-panel spacing used in \pkg{multipanelfigure} in inches.
#' @param max_height_in_in Maximal figure height in inches.
#' @return A \code{\link{numeric}} representing the figure height calculated.
#'
#' @name estimate_figure_height-deprecated
#' @usage estimate_figure_height(height_in_panels, panel_height_in_in = 2.25,
#' axis_space_in_in = 0.2,
#' mpf_row_space = as.numeric(grid::convertUnit(grid::unit(5, 'mm'), 'in')),
#' max_height_in_in = 9)
#' @seealso \code{\link{ReporteR.base-deprecated}}
#' @keywords internal
NULL

#' @rdname ReporteR.base-deprecated
#' @section \code{estimate_figure_height}:
#' Refer to \code{\link{figure_dimension}}/\code{\link{figure_height}}/\code{\link{figure_width}}
#' instead.
#' @export
estimate_figure_height <- function(
  height_in_panels,
  panel_height_in_in = 2.25,
  axis_space_in_in = 0.2,
  mpf_row_space = as.numeric(grid::convertUnit(grid::unit(5, 'mm'), 'in')),
  max_height_in_in = 9)
{
  .Deprecated('figure_height')
  height_in_panels %>%
    assertive.numbers::assert_all_are_whole_numbers()
  panel_height_in_in %>%
    assertive.types::assert_is_a_number()
  axis_space_in_in %>%
    assertive.types::assert_is_a_number()
  mpf_row_space %>%
    assertive.types::assert_is_a_number()
  max_height_in_in %>%
    assertive.types::assert_is_a_number()

  sub_figure_height <- height_in_panels %>%
    # Panel-derived height
    magrittr::multiply_by(panel_height_in_in) %>%
    # x-axis label-derived height
    magrittr::add(axis_space_in_in)

  global_height <- sub_figure_height %>%
    # mpf row spacing-derived height
    magrittr::add(mpf_row_space) %>%
    sum(na.rm = TRUE)
  tmp_panel_sum <- NaN
  if(global_height > max_height_in_in)
  {
    global_height <- max_height_in_in
    tmp_panel_sum <- global_height %>%
      magrittr::subtract(
        height_in_panels %>%
          length() %>%
          magrittr::multiply_by(mpf_row_space))
    sub_figure_height %<>%
      sapply(
        function(x){
          tmp_panel_sum / sum(sub_figure_height, na.rm = TRUE) * x
        })
  }
  return(
    list(
      tmp_panel_sum = tmp_panel_sum,
      global = global_height,
      sub    = sub_figure_height))
}