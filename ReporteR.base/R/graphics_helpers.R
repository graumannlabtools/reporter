#' @name graphics_helpers
#' @title Helper functions for figure seizing
#' @details Especially in the context of using \pkg{multipanelfigure}, but also
#' for general use in \pkg{rmarkdown}/\pkg{bookdown} documents, it is of
#' interest to calculate image dimensions based on the size of the canvas,
#' respectively the space on it available for content.
#'
#' The underlying generic function \code{figure_dimension} proceeds as follows:
#' \itemize{
#' \item A maximal dimension is determined from from the total canvas dimension
#' (\code{canvas_dim} minus margins (\code{margin_width}). The height result
#' is further adjusted using \code{max_relative_dim}, a step designed to factor
#' in  space for legends etc..
#' \item Based on the number of panels to be arranged (\code{n_panels}), an
#' output dimension is generated either a) from a fixed requested panel
#' dimension (\code{panel_dim}) or b) from a fraction (\code{rel_panel_dim}) of
#' the the maximal dimension calculated above.
#' \item The calculated output diemsion is limited to the maximally available
#' space.
#' \item Finally, output is unit-converted using and returned as a
#' \code{\link{numeric}}.}
#' \code{paper_formats} represents a list of \code{\link[grid]{unit}} objects
#' describing commonly used paper format(s).
#' @param canvas_dim A single \code{\link[grid]{unit}} object representing the
#' seize of the available canvas;
#' @param canvas_height \code{canvas_dim} in the context of height estimation;
#' @param canvas_width \code{canvas_dim} in the context of width estimation;
#' @param margin_width One or two \code{\link[grid]{unit}} objects
#' representing the margins deducted from the totally available canvas; if
#' \code{length(margin_width_h) == 1}, symmetric margins are assumed;
#' @param max_relative_dim Single \code{\link{numeric}} object representing
#' the fraction of the totally content-available canvas space a figure should
#' maximally use (useful with legends etc.);
#' @param max_relative_height \code{max_relative_dim} in the context of height estimation;
#' @param max_relative_width \code{max_relative_dim} in the context of width estimation;
#' @param n_panels Single whole \code{\link{numeric}} object representing the
#' number of panels to be used in the figure in question (in the relevant
#' dimension);
#' @param panel_dim Single \code{\link{numeric}} object representing the space a
#' panel should optimally use (incompatible with \code{rel_panel_dim});
#' @param panel_height \code{panel_dim} in the context of height estimation;
#' @param panel_width \code{panel_dim} in the context of width estimation;
#' @param rel_panel_dim Single \code{\link{numeric}} object representing the
#' fraction of the totally content-available canvas a panel should optimally use
#' (incompatible with \code{panel_dim});
#' @param rel_panel_height \code{rel_panel_dim} in the context of height estimation;
#' @param rel_panel_width \code{rel_panel_dim} in the context of width estimation;
#' @param unit_to Single \code{\link{character}} object representing a legal
#' \code{\link[grid]{unit}} name the result of the calculation is converted to;
#' @author Johannes Graumann
#' @return The calculated result as s single \code{\link{numeric}} object.
#' @examples
#' paper_formats$dinA4_portrait$width
#' # Default figure width (inch) in an Din A4 portrait context
#' figure_width()
#' @rdname graphics_helpers
#' @export
figure_dimension <- function(
  canvas_dim,
  margin_width = grid::unit(1, 'in'),
  max_relative_dim = 1,
  n_panels = 1,
  panel_dim,
  rel_panel_dim = NULL,
  unit_to = 'in')
{
  # Check prerequisites -----------------------------------------------------
  assert_all_are_grid_units(canvas_dim)
  assertive.properties::assert_is_of_length(canvas_dim, 1)

  assert_all_are_grid_units(margin_width)
  assertive.sets::assert_is_subset(length(margin_width), c(1,2))

  assertive.types::assert_is_a_number(max_relative_dim)
  assertive.numbers::assert_all_are_in_left_open_range(max_relative_dim, lower = 0, upper = 1)

  assertive.types::assert_is_a_number(n_panels)
  assertive.numbers::assert_all_are_whole_numbers(n_panels)
  assertive.numbers::assert_all_are_greater_than_or_equal_to(n_panels, 1)

  if(!is.null(panel_dim))
  {
    assertive.types::assert_is_a_number(panel_dim)
    assert_all_are_grid_units(panel_dim)
    assertive.numbers::assert_all_are_greater_than(panel_dim, 0)
  }

  if(!is.null(rel_panel_dim))
  {
    assertive.types::assert_is_a_number(rel_panel_dim)
    assertive.numbers::assert_all_are_in_left_open_range(rel_panel_dim, lower = 0, upper = 1)
  }

  assertive.properties::assert_is_of_length(c(panel_dim, rel_panel_dim), 1)

  # no check for legal grid units available

  # Processing --------------------------------------------------------------
  # What to deduct due to margins
  if(length(margin_width) == 1) margin_width %<>%  rep(times = 2)
  margin_width %<>%
    sum()

  # Calculate maximum dimension
  max_dim <- canvas_dim %>%                 # Canvas size
    magrittr::subtract(margin_width) %>%    # reduce by margin size
    magrittr::multiply_by(max_relative_dim) # Reduce to max_relative_dim

  # Calculate figure dimension
  if(!is.null(panel_dim))
  {
    out_dim <- n_panels %>%            # number of panels to be arranged
      magrittr::multiply_by(panel_dim) # multiplied by requested panel dimension
  } else {
    out_dim <- max_dim %>%                     # maximal dimension
      magrittr::multiply_by(rel_panel_dim) %>% # multiplied by requested relative dimension
      magrittr::multiply_by(n_panels)          # number of panels to be arranged
  }
  out_dim %<>%
    min(max_dim) %>%                        # limit to max dimension
    grid::convertUnit(unitTo = unit_to) %>% # convert to requested unit
    as.numeric()                            # numerizise

  return(out_dim)
}

#' @rdname graphics_helpers
#' @export
figure_height <- function(
  canvas_height = paper_formats[['dinA4_portrait']][['height']],
  margin_width = grid::unit(1, 'in'),
  max_relative_height = 0.85,
  n_panels = 1,
  panel_height = figure_width(rel_panel_width = 1, unit_to = unit_to) * 2/3,
  rel_panel_height = NULL,
  unit_to = 'in')
{
  return(
    figure_dimension(
      canvas_dim = canvas_height,
      margin_width = margin_width,
      max_relative_dim = max_relative_height,
      n_panels = n_panels,
      panel_dim = panel_height,
      rel_panel_dim = rel_panel_height,
      unit_to = unit_to))
}

#' @rdname graphics_helpers
#' @export
figure_width <- function(
  canvas_width = paper_formats[['dinA4_portrait']][['width']],
  margin_width = grid::unit(1, 'in'),
  max_relative_width = 1,
  n_panels = 1,
  panel_width = NULL,
  rel_panel_width = min(1, n_panels * 2/3),
  unit_to = 'in')
{
  return(
    figure_dimension(
      canvas_dim = canvas_width,
      margin_width = margin_width,
      max_relative_dim = max_relative_width,
      n_panels = n_panels,
      panel_dim = panel_width,
      rel_panel_dim = rel_panel_width,
      unit_to = unit_to))
}

#' @rdname graphics_helpers
#' @name paper_formats
#' @docType data
#' @keywords data
NULL

utils::globalVariables('paper_formats')