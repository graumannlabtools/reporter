Phosphopeptide enrichment was performed using a two step protocol.
First strong cation exchange (SCX) chromatography was performed on
an Äkta purifier 10 (GE Healthcare) using a 1 ml Resource S column
(GE Healtcare) as described [@beausoleil_2004]. Peptide samples
were acidified and adjusted to 30% acetonitrile by mixing with
solvent A (7 mM KH~2~PO~4~, 30% acetonitrile, pH 2.65) and water.
After loading the sample on the column, peptides were sequentially
eluted by applying the following gradient: 7 min from 100% solvent
A to 8% solvent B (7 mM KH~2~PO~4~, 350 mM KCl, 30% acetonitrile,
pH 2.65), 45 min to 11% solvent B, 5 min to 20% solvent B, 15 min
to 39% solvent B, 8 min to 60% solvent B, followed finally by 10 min
at 100% solvent B. Fractions of 1 ml each were collected, pooled
according to the chromatogram and reduced in a SpeedVac concentrator
to a final volume of approximately 250 µl. In a second step, the
fractions were subjected to TiO~2~ chromatography in batch mode
[@hilger_2009, modified]. To this end, titansphere TiO beads (10 µm;
GL Sciences Inc.) were washed sequentially with elution buffer
(3.75% NH~3~, 40% acetonitrile), 70% acetonitrile and wash buffer
(80% acetonitrile, 1% TFA), and equilibrated in binding buffer (80%
acetonitrile, 6% TFA). SCX chromatography fractions and the
corresponding flow--through were adjusted to a final concentration of
80% acetonitrile and 6% TFA, mixed with TiO~2~ beads (5 mg for the
flow--through, 2.5 mg per fraction) and incubated rotating for 25 min.
Phosophopeptide--depletion was sequentially performed three times for
the flow--through and twice for each SCX fraction. Subsequently, TiO~2~
beads were washed twice with binding buffer and transferred to a 200 µl
pipette tip fritted with a C8--membrane plug. Extractions for each SCX
fraction as well as the 2nd and 3rd extractions of the flow--through
were pooled prior to washing with binding and wash buffer. Peptides
were sequentially eluted 3 times from TiO~2~ beads with 25 µl elution
buffer each, dried in a SpeedVac concentrator and reconstituted with
0.1% formic acid for LC-MS/MS analysis.
