## In--Solution Protein Digest

Protein (`r local_params$protein_amount`) was dissolved in urea
buffer (6 M urea, 2 M thiourea, 10 mM HEPES, pH 8.0) and enzymatic
peptidolysis was performed by in--solution digestion [@graumann_2008].

In brief, protein disulfide bonds were reduced using 10 mM
dithiothreitol and cysteins alkylated using 55 mM iodoacetamide.
Subsequently, proteins were digested using Lys--C
(`r local_params$lysc_enzyme_to_protein_ratio` enzyme--to--protein ratio;
Wako Chemicals GmbH) at room temperature for 3 hours, followed by
an overnight trypsin treatment (`r local_params$tryp_enzyme_to_protein_ratio`
enzyme--to--protein ratio; Serva) at room temperature.
`r if(local_params$cleanup == 'stage_tip'){'The resulting
peptide mixture was desalted, concentrated and stored on "stop and go
extraction" (STAGE) tips [@rappsilber_2003].'}`
`r if(local_params$cleanup == 'oligo_r3'){'The resulting
peptide mixture was desalted and concentrated using R3 columns [@billing_2017].'}`
