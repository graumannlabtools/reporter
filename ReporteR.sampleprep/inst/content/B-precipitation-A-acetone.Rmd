Solubilized proteins were precipitated by addition of four sample volumes
of 100% acetone, followed by incubation at $-20$°C `r local_params$incubation_time`
and pelleting at 15,000 g for 10 minutes. After washing the resulting pellet
with 90% acetone, samples were dried to remove residual acetone.