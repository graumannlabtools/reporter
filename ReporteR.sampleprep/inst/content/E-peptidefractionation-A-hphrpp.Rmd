To analyze the proteome an in the interest of deeper sequencing in the context
of mass spectrometry's dynamic range limitation, sample complexity was reduced
by separating `r local_params$peptide_amount` of total peptide into
`r ReporteR.base::verbalize_integers(local_params$n_fractions)` fractions using
the High pH Reversed--Phase Peptide Fractionation Kit (Pierce; according to the
manufacturer's protocol), followed by final desalting, concentration and
storage on "stop and go extraction" (STAGE) tips [@rappsilber_2003].
