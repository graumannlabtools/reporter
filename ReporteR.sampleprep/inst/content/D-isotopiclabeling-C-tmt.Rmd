## Chemical Peptide--Labeling for Quantitation using Tandem Mass Tags (TMT)
```{r sampleprep-appendix-isotopiclabeling-A-rdm-parameval, echo = FALSE}
if(!local_params$multiplexing %in% seq(2, 6))
{
  stop('Unimplemented multiplexing:', local_params$multiplexing)
}
label_string <- paste(seq(126,131)) %>%
  head(n = local_params$multiplexing)
  
label_string %<>%
  ReporteR.base::itemize(sort = FALSE, and = 'or') %>%
  paste('Th')

if(length(local_params$labeled_conditions_increasing) == 1 &&
   is.na(local_params$labeled_conditions_increasing))
{
  conditions_string <- ''
} else {
  if(length(local_params$labeled_conditions_increasing) != local_params$multiplexing)
  {
    stop("Length of 'labeled_conditions_increasing' must correspond to 'multiplexing'.")
  }
  conditions_string <- paste0(
    ' for ',
    local_params$labeled_conditions_increasing %>%
      ReporteR.base::itemize(sort = FALSE),
    ', respectively')
}
```

Following digestion, the sample was acidified to a final concentration of 0.5% formic
acid and 2.5% trifluroric acid and subjected to reverse--phase extraction using OLIGO
R3 resin (ThermoFisher Scientific). Recovered peptides were dissolved in 50 mM
triethyl ammonium bicarbonate (TEAB) and peptide concentration was measured using the
Fluorimetric Peptide Assay (Pierce). Samples containing an equal amount of peptides
were subjected to tandem mass tag [TMT; @thompson_2003], in--solution labeling protocol,
as described before [@thompson_2003; @wuhr_2012] with the following change: the 
TMT reagent--to--peptide ratio employed was
`r round(local_params$reagent_to_peptide_ratio, digits = 2)`. TMTs conferring
quantitation ions of mass `r label_string` were used`r conditions_string`.
`r ifelse(local_params$label_test, 'Labeling efficiency was confirmed to be $>95$% by LC/MS^2^.', '')`
Differentially labeled samples were mixed at peptide equivalents and desalted.
