```{r parameters-and-defaults, include = FALSE}
module <- "sampleprep"
section <- "appendix-phosphoproteomics"
parameters_and_defaults <- list()

parameters_and_defaults <- list(
  protocol = structure(
    'tish',
    type = 'character',
    choices = c('tish', 'beausoleil_hilger'),
    several.ok = FALSE),
  desalting = structure(
    'seppak',
    type = 'character',
    choices = c('seppak', 'r3'),
    several.ok = FALSE))
```
```{r parameter-merge, include = FALSE}
local_params <- module %>%
  getOption() %>%
  magrittr::extract2(section) %>%
  ReporteR.base::validate_params(parameters_and_defaults)
```
## Phospho--Peptide Enrichment

```{r sampleprep-appendix-appendix-phosphoproteomics-bh, echo = FALSE, child = system.file(file.path('content', 'F-phosphoproteomics-A-tish.Rmd'), package = 'ReporteR.sampleprep', mustWork = TRUE), R.options = params, eval = ifelse(exists('local_params'), local_params$protocol == 'tish', FALSE)}
```

```{r sampleprep-appendix-appendix-phosphoproteomics-bh, echo = FALSE, child = system.file(file.path('content', 'F-phosphoproteomics-B-bh.Rmd'), package = 'ReporteR.sampleprep', mustWork = TRUE), R.options = params, eval = ifelse(exists('local_params'), local_params$protocol == 'beausoleil_hilger', FALSE)}
```

```{r sampleprep-appendix-lysis-terminal-cleanup, include = FALSE}
ReporteR.base::purge_nonpersistent()
```
