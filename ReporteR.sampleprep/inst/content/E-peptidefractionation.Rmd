```{r parameters-and-defaults, include = FALSE}
module <- "sampleprep"
section <- "appendix-peptidefractionation"
parameters_and_defaults <- list()

parameters_and_defaults <- list(
  protocol = structure(
    'high_pH_rp_pierce',
    type = 'character',
    choices = c('high_pH_rp_pierce'),
    several.ok = FALSE),
  peptide_amount = structure(
    '100 µg',
    type = 'character',
    choices = NA,
    several.ok = FALSE),
  n_fractions = structure(
    8,
    type = 'numeric',
    choices = NA,
    several.ok = FALSE))
```
```{r parameter-merge, include = FALSE}
local_params <- module %>%
  getOption() %>%
  magrittr::extract2(section) %>%
  ReporteR.base::validate_params(parameters_and_defaults)
```
## Peptide Fractionation

```{r sampleprep-appendix-appendix-peptidefractionation-hphrpp, echo = FALSE, child = system.file(file.path('content', 'E-peptidefractionation-A-hphrpp.Rmd'), package = 'ReporteR.sampleprep', mustWork = TRUE), R.options = params, eval = ifelse(exists('local_params'), local_params$protocol == 'high_pH_rp_pierce', FALSE)}
```

```{r sampleprep-appendix-lysis-terminal-cleanup, include = FALSE}
ReporteR.base::purge_nonpersistent()
```
